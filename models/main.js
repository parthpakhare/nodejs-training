const {Sequelize, DataTypes} = require('sequelize')

const sequelize = new Sequelize('project', 'root', 'password',{
    host: 'localhost',
    dialect: 'mysql'
})

sequelize.authenticate()
.then(() => {
    console.log("connected")
    console.log("main.js")
})
.catch(err => {
    console.log("Error", err)
})

const db = {}
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require('./users')(sequelize, DataTypes)

db.sequelize.sync({force: false})
.then(() => {
    console.log("Table Synced")
    console.log("DB",db.users)
})

