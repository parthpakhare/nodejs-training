const express = require('express');

bodyParser = require('body-parser');

require('./models/main')


const app = express();

app.use(bodyParser.json());

const port = 3000

var userCtrl = require('./controllers/userController')


app.get('/', (req, resp) => {
    resp.sendFile("form.html", { root: __dirname });
    //resp.send(path.join(__dirname+'./form.html'));
})

app.post('/add', userCtrl.addUser)
app.delete('/del', userCtrl.deldata)
app.get('/showall', userCtrl.showall)
/* app.get('/show',(req,resp) => {
    resp.sendFile("form.html", { root: __dirname });
    console.log("Response",resp)
}) */
app.post('/show', (req,res) => {
    //userCtrl.show,
    res.send(req.body)
    console.log("Request",req.body)
})
app.put('/updatedata', userCtrl.updatedata)

app.listen(port, () => {
    console.log("App listening on port 3000")
})