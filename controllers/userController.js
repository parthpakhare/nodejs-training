var db = require('../models/main')
const {Sequelize, DataTypes} = require('sequelize')

const sequelize = new Sequelize('project', 'root', 'password',{
    host: 'localhost',
    dialect: 'mysql'
})

sequelize.authenticate()
.then(() => {
    console.log("connected")
    console.log("userController.js")
})
.catch(err => {
    console.log("Error", err)
})

//const sequelize = db.sequelize

const Users = sequelize.define('users', {

    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
},{
    timestamps: false
    //tableName: 'userdata'
});

//console.log("Output Data", data.json)



var addUser = async (req, resp) => {
    
    /* let response = {
        data: "Ok"
    } */
    
    //const data = await Users.create({firstName: 'Jack2', lastName: 'Sparrow2'})
    
    //const data = await Users.create({firstName: 'ParthP'})

    const data = await Users.bulkCreate([
        {firstName: 'Bulk', lastName: 'Entry1'},
        {firstName: 'Bulk', lastName: 'Entry2'},
        {firstName: 'Bulk', lastName: 'Entry3'}
    ]);
    
    resp.status(200).json(data)

    console.log("Data entered", data)
}


var deldata = async (req,resp) => {
    
    const data = await Users.destroy({
        where: {
            id: 3
        }
    });
    
    resp.status(200).json(data)

}

var showall = async (req, resp) => {
    
    const data = await Users.findAll({})
    
    resp.status(200).json(data)

}

var show = async (req, resp) => {
    
    const data = await Users.findOne({
        where: {
            id: 2
        }
    })

    resp.status(200).json(data)

}

var showfn = async (req, resp) => {

    const data = await Users.findOne({
        where: {
            id: 2
        }
    })

    resp.status(200).json(data)

}

var updatedata = async (req, resp) => {
    
    const data = await Users.update({ lastName: "Something" }, {
        where: {
            lastname: "Sonething"
        }
    })

}


/* var crud = async (req, resp) => {

    try{
        const data = await Users.create({firstName: 'Jack', lastName: 'Sparrow'})
    }
    catch(err) {
        console.log(err)
    }   

    resp.status(200),json(data)

    console.log("Data entered", data)
} */

module.exports = { 
    //data,
    deldata,
    addUser,
    showall,
    show,
    updatedata,
}